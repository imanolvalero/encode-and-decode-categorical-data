def is_binary(num: int) -> bool:
    return all(x in '01' for x in str(num))

def is_palindrome(num: int) -> bool:
    text = str(num)
    return text == text[::-1]


def is_prime(num: int) -> bool:
    from sympy import isprime

    return isprime(num)
