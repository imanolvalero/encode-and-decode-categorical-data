from checks import is_binary, is_palindrome, is_prime


def categorize(num: int) -> int:
    checks = [is_binary, is_palindrome, is_prime]
    return sum(
        2**(i+1) 
        for i, check in enumerate(checks)
        if check(num)
    )


def create_prompt(result: int) -> str:
    if not result:
        return 'nothing'

    categories = 'binary palindrome prime'.split(' ')
    texts = [
        categories[i] for i in range(len(categories)) if result & 2**(i+1)
    ]
    return ' and'.join(', '.join(texts).rsplit(',', 1))


def analyze(num: int):
    categories = categorize(num)
    print(f"Number {num} is {create_prompt(categories)}")
